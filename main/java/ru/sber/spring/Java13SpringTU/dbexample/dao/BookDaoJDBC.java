package ru.sber.spring.Java13SpringTU.dbexample.dao;

import ru.sber.spring.Java13SpringTU.dbexample.db.DBApp;
import ru.sber.spring.Java13SpringTU.dbexample.model.Book;
import java.sql.*;

public class BookDaoJDBC {
    public Book findBookById(Integer bookId) { // поиск книги по "id" в Базе Данных
        try(Connection connection = DBApp.INSTANCE.getConnection()) {
        // подключаемся к БД:
            if (connection != null) {
                System.out.println("Ура, я подключился к БД!");
            } else {
                System.out.println("БД отдыхает...");
            }

        // Пишем запрос в БД:
            PreparedStatement sqlQuery = connection.prepareStatement("select * from books where id = ?");
            sqlQuery.setInt(1, bookId);

        // Обрабатываем ответ (результат):
            ResultSet resultSet = sqlQuery.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();
                book.setBookTitle(resultSet.getString("title"));
                book.setAuthor(resultSet.getString("author"));
                book.setDateAdded(resultSet.getDate("date_added"));
                return book; // получили Книгу в результате (1)
            }
        }
        catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return null; // это на случай ошибки (2)
    }
}












