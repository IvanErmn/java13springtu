package ru.sber.spring.Java13SpringTU.dbexample.dao;

import org.springframework.stereotype.Component;
import ru.sber.spring.Java13SpringTU.dbexample.model.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDaoBean {
    private final Connection connection;

    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(Integer bookId) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement("select * from books where id = ?"); // Пишем запрос в БД:
        sqlQuery.setInt(1, bookId);

        ResultSet resultSet = sqlQuery.executeQuery(); // Обрабатываем ответ (результат):
        Book book = new Book();

// Получаем и выводим в консоль результат:
        while (resultSet.next()) {
//            book.setBookId(resultSet.getInt("id"));
            book.setBookTitle(resultSet.getString("title"));
            book.setAuthor(resultSet.getString("author"));
            book.setDateAdded(resultSet.getDate("date_added"));
        }
        return book;
    }

    public Book findBookByTitle(String bookTitle) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement("select id from books where title = ?");
        sqlQuery.setString(1, bookTitle);

        ResultSet resultSet = sqlQuery.executeQuery();
        resultSet.next();
        return findBookById(resultSet.getInt("id"));
    }
}
