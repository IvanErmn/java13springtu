package ru.sber.spring.Java13SpringTU.dbexample.constants;

public interface DBConstants {
//    jdbc:postgresql://localhost:5432/local_db
    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
