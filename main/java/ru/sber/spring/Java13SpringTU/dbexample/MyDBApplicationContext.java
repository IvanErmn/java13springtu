package ru.sber.spring.Java13SpringTU.dbexample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.sber.spring.Java13SpringTU.dbexample.dao.BookDaoBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.sber.spring.Java13SpringTU.dbexample.constants.DBConstants.*;

@Configuration // пометка, чтобы "подключение" поместилось в КОНТЕЙНЕР
@ComponentScan // 

public class MyDBApplicationContext {

    @Bean // пометка, чтобы был доступин везде
    @Scope("singleton") // должен быть ЕДИНСТВЕННЫМ

    public Connection newConnection() throws SQLException { // будет всегда доступен из других классов, т.к. помечен @Bean
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                USER,
                PASSWORD);
    }

// Создаем BookDaoBean:
//    @Bean
//    public BookDaoBean bookDaoBean() throws SQLException {
//        return new BookDaoBean((newConnection()));
//    }
}
