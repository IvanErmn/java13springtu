// [JAVA13][ДЗ№6] - задача на проверку от Еремина Ивана Романовича

package ru.sber.spring.Java13SpringTU.dbexample.model;

import lombok.*;

import java.util.Date;
import java.util.List;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String surname;
    private String name;
    private String birthDay;
    private String phoneNumber; // телефон
    private String eMail; // почта
    private List<Book> books; // список названий книг
}
