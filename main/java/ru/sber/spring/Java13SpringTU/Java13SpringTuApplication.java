package ru.sber.spring.Java13SpringTU;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.sber.spring.Java13SpringTU.dbexample.dao.BookDaoBean;
import ru.sber.spring.Java13SpringTU.dbexample.dao.BookDaoJDBC;
import ru.sber.spring.Java13SpringTU.dbexample.dao.UserDaoBean;
import ru.sber.spring.Java13SpringTU.dbexample.model.User;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
@EnableScheduling
public class Java13SpringTuApplication implements CommandLineRunner /*(интерфейс для запуска web приложения*/ {

// Theory:
	private NamedParameterJdbcTemplate jdbcTemplate; //
	private BookDaoBean bookDaoBean;
	@Autowired
	public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	@Autowired
	public void setBookDaoBean(BookDaoBean bookDaoBean) {
		this.bookDaoBean = bookDaoBean;
	}
	public static void main(String[] args) {
		SpringApplication.run(Java13SpringTuApplication.class, args);

	}


// [JAVA13][ДЗ№6] - задача на проверку от Еремина Ивана Романовича
	private UserDaoBean userDaoBean;
	@Autowired
	public void setUserDaoBean(UserDaoBean userDaoBean) {
		this.userDaoBean = userDaoBean;
	}

	@Override
	public void run(String... args) throws SQLException {
//		try {
//			List<Book> books1 = new ArrayList<>();
//			books1.add(bookDaoBean.findBookById(1));
//			books1.add(bookDaoBean.findBookById(6));
//			books1.add(bookDaoBean.findBookById(7));
//			User user1 = new User("Иванов", "Иван", "13-04-2000",
//					"8(900)432-12-12","ivanov-i@mail.ru", books1);
//			User user2 = new User("Арсентьев", "Артем", "12-03-1965",
//					"8(900)531-22-22","fdsgs-i@mail.ru", books1);
//
//			userDaoBean.addUser(user1);
//			userDaoBean.addUser(user2);
//			userDaoBean.addUser(new User("Соловьева", "Дарья", "31-12-1987",
//					"8(932)312-00-00","fhdahsdd@mail.ru", null));
//			userDaoBean.addUser(new User("Еремина", "Дарья", "31-12-1987",
//					"8(932)242-00-00","fhdahsdd@mail.ru", null));
//
//			userDaoBean.printBooksByUser("8(900)432-12-12");
//			userDaoBean.printBooksByUser("8(900)531-22-22");
//			userDaoBean.printBooksByUser("8(932)312-00-00");
//
//			System.out.println("\nПроверка 1 вызова списка книг через номер телефона:");
//			for (String e : userDaoBean.listBooksOfUser("8(900)432-12-12")) {
//				System.out.println(" - " + e);
//			}
//
//			System.out.println("\nПроверка 2 вызова списка книг через номер телефона:");
//			for (String e : userDaoBean.listBooksOfUser("8(900)432-12-12")) {
//				System.out.println(" - " + bookDaoBean.findBookByTitle(e));
//			}
//
////			System.out.println("\n------------------------------------------------------------------------\nСписок книг:");
////			List<Book> books = jdbcTemplate.query("select * from books",
////				(rs, rowNum) -> new Book(
////						rs.getInt("id"),
////						rs.getString("title"),
////						rs.getString("author"),
////						rs.getDate("date_added")
////				));
////			books.forEach(System.out::println);
//		}
//		catch (SQLException e) {
//			System.out.println("Error: " + e.getMessage());
//		}

// JDBC:
//		BookDaoJDBC bookDaoJDBC = new BookDaoJDBC();
//		System.out.print("\nJDBC: ");
//		System.out.println(bookDaoJDBC.findBookById(1));
//
//// Bean:
//		System.out.println("\nBean: ");
//		System.out.println(bookDaoBean.findBookById(1));
//
////
//		System.out.println("\nСписок книг (jdbcTemplate.query): ");
//		List<Book> books = jdbcTemplate.query("select * from books",
//				(rs, rowNum) -> new Book(
//						rs.getInt("id"),
//						rs.getString("title"),
//						rs.getString("author"),
//						rs.getDate("date_added")
//				));
//		books.forEach(System.out::println);
	}
}






























