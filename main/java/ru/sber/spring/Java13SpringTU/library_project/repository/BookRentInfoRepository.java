package ru.sber.spring.Java13SpringTU.library_project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import ru.sber.spring.Java13SpringTU.library_project.model.BookRentInfo;

@Repository
public interface BookRentInfoRepository extends GenericRepository<BookRentInfo> {

    Page<BookRentInfo> getBookRentInfoByUserId(Long userId, Pageable pageable);

}