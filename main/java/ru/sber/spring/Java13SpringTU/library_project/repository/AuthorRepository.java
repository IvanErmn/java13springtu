package ru.sber.spring.Java13SpringTU.library_project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.spring.Java13SpringTU.library_project.model.Author;

@Repository
public interface AuthorRepository extends GenericRepository<Author> {
    Page<Author> findAllByIsDeletedFalse(Pageable pageable);

    Page<Author> findAllByAuthorFioContainsIgnoreCase(String fio, Pageable pageable);

    @Query(value = """
          select case when count(a) > 0 then false else true end
          from Author a join a.books b
                        join BookRentInfo bri on b.id = bri.book.id
          where a.id = :authorId
          and bri.returned = false
          """)
    boolean checkAuthorForDeletion(final Long authorId);
}
