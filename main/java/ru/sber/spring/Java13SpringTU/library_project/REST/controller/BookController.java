package ru.sber.spring.Java13SpringTU.library_project.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookDTO;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;
import ru.sber.spring.Java13SpringTU.library_project.service.BookService;

@RestController
@RequestMapping("/books") //localhost:9090/api/rest/books
@Tag(name = "Книги", description = "Контроллер для работы с книгами библиотеки")
public class BookController extends GenericController<Book, BookDTO> {
    public BookController(BookService bookService) {
        super(bookService);
    }

//    @Operation(description = "Добавить автора к книге", method = "addAuthor")
//    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                          @RequestParam(value = "authorId") Long authorId) {
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
//        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
//        book.getAuthors().add(author);
//        return ResponseEntity.status(HttpStatus.OK).body(bookRepository.save(book));
//    }
}
