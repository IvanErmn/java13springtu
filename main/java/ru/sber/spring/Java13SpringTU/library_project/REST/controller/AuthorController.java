package ru.sber.spring.Java13SpringTU.library_project.REST.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.Java13SpringTU.library_project.dto.AuthorDTO;
import ru.sber.spring.Java13SpringTU.library_project.model.Author;
import ru.sber.spring.Java13SpringTU.library_project.service.AuthorService;

@RestController
@RequestMapping("/authors") //localhost:9090/api/rest/books
@Tag(name = "Авторы", description = "Контроллер для работы с авторами книг библиотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthorController extends GenericController<Author, AuthorDTO> {
    private AuthorService authorService;
    public AuthorController(AuthorService authorService) {
        super(authorService);
        this.authorService = authorService;
    }

//    @Operation(description = "Добавить книгу к автору", method = "addBook")
//    @RequestMapping(value = "/addBook", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Author> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                          @RequestParam(value = "authorId") Long authorId) {
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
//        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
//        author.getBooks().add(book);
////        book.getAuthors().add(author);
//        return ResponseEntity.status(HttpStatus.OK).body(authorRepository.save(author));
//    }
}
