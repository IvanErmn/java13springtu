package ru.sber.spring.Java13SpringTU.library_project.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookRentInfoDTO;
import ru.sber.spring.Java13SpringTU.library_project.model.BookRentInfo;
import ru.sber.spring.Java13SpringTU.library_project.service.BookRentInfoService;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда книг",
        description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
public class RentBookController extends GenericController<BookRentInfo, BookRentInfoDTO> {
    public RentBookController(BookRentInfoService bookRentInfoService) {
        super(bookRentInfoService);
    }
}
