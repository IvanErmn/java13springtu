package ru.sber.spring.Java13SpringTU.library_project.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
