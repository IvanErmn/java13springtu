package ru.sber.spring.Java13SpringTU.library_project.constants;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}
