package ru.sber.spring.Java13SpringTU.library_project.constants;

public interface FileDirectoriesConstants {
    String BOOKS_UPLOAD_DIRECTORY = "files/books";
}
