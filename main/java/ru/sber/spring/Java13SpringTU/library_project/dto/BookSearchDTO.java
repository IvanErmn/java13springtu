package ru.sber.spring.Java13SpringTU.library_project.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sber.spring.Java13SpringTU.library_project.model.Genre;

@Getter
@Setter
@ToString
public class BookSearchDTO {
    private String title;
    private String authorFio;
    private Genre genre;
}