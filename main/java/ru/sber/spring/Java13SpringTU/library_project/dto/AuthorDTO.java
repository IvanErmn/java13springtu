package ru.sber.spring.Java13SpringTU.library_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class AuthorDTO extends GenericDTO {
    private String authorFio;
    private String birthDate;
    private String description;
    private Set<Long> booksIds;
    private boolean isDeleted;
}
