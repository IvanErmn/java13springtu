package ru.sber.spring.Java13SpringTU.library_project.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.Java13SpringTU.library_project.model.Author;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;
import ru.sber.spring.Java13SpringTU.library_project.model.Genre;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class BookDTO extends GenericDTO {
    private String title;
    private String description;
    private String localDate;
    private String publisher;
    private Integer pageCount;
    private String storagePlace;
    private String onlineCopyPath;
    private Integer amount;
    private Genre genre;
    private Set<Long> authorsIds;
    private boolean isDeleted;

    public BookDTO(Book book) {
        BookDTO bookDTO = new BookDTO();
        //из entity делаем DTO
        bookDTO.setTitle(book.getTitle());
        bookDTO.setGenre(book.getGenre());
        bookDTO.setDescription(book.getDescription());
        bookDTO.setId(book.getId());
        bookDTO.setPageCount(book.getPageCount());
        bookDTO.setLocalDate(book.getLocalDate().toString());
        Set<Author> authors = book.getAuthors();
        Set<Long> authorIds = new HashSet<>();
        if (authors != null && authors.size() > 0) {
            authors.forEach(a -> authorIds.add(a.getId()));
        }
        bookDTO.setAuthorsIds(authorIds);
    }
}
