package ru.sber.spring.Java13SpringTU.library_project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookWithAuthorsDTO extends BookDTO {
    private Set<AuthorDTO> authors;
    public BookWithAuthorsDTO(Book book, Set<AuthorDTO> authors) {
        super(book);
        this.authors = authors;
    }
}
