package ru.sber.spring.Java13SpringTU.library_project.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddBookDTO {
    Long bookId;
    Long authorId;
}

