package ru.sber.spring.Java13SpringTU.library_project.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDTO extends GenericDTO {
    private String login;
    private String password;
    private String email;
    private String birthDate;

    private String firstName;
    private String lastName;
    private String middleName;

    private String phone;
    private String address;

    private String changePasswordToken;

    private RoleDTO role;
    private Set<Long> userBooksRent;
    private boolean isDeleted;
}
