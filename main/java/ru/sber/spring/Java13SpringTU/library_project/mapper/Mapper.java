package ru.sber.spring.Java13SpringTU.library_project.mapper;

import ru.sber.spring.Java13SpringTU.library_project.dto.GenericDTO;
import ru.sber.spring.Java13SpringTU.library_project.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    List<E> toEntities(List<D> dtos);
    D toDTO(E entity);
    List<D> toDTOs(List<E> entities);
}