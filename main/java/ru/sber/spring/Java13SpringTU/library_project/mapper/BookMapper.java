package ru.sber.spring.Java13SpringTU.library_project.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookDTO;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;
import ru.sber.spring.Java13SpringTU.library_project.model.GenericModel;
import ru.sber.spring.Java13SpringTU.library_project.repository.AuthorRepository;
import ru.sber.spring.Java13SpringTU.library_project.utils.DateFormatter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BookMapper extends GenericMapper<Book, BookDTO>{
    private final AuthorRepository authorRepository;
//    private final ModelMapper modelMapper;
    protected BookMapper(ModelMapper modelMapper, AuthorRepository authorRepository) {
        super(modelMapper, Book.class, BookDTO.class);
        this.authorRepository = authorRepository;
//        this.modelMapper = modelMapper;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Book.class, BookDTO.class)
                .addMappings(m -> m.skip(BookDTO::setAuthorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(BookDTO.class, Book.class)
                .addMappings(m -> m.skip(Book::setAuthors)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Book::setLocalDate)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(BookDTO source, Book destination) {
        if(!Objects.isNull(source.getAuthorsIds())){
            destination.setAuthors(new HashSet<>(authorRepository.findAllById(source.getAuthorsIds())));
        } else {
            destination.setAuthors(null);
        }
        destination.setLocalDate(DateFormatter.formatStringToDate(source.getLocalDate()));
    }
    @Override
    protected void mapSpecificFields(Book source, BookDTO destination) {
        destination.setAuthorsIds((getIds(source)));
    }
    @Override
    protected Set<Long> getIds(Book book) {
        return Objects.isNull(book) || Objects.isNull(book.getAuthors())
                ? null
                : book.getAuthors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}