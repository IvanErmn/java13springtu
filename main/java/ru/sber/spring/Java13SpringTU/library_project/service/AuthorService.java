package ru.sber.spring.Java13SpringTU.library_project.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.Java13SpringTU.library_project.constants.Errors;
import ru.sber.spring.Java13SpringTU.library_project.dto.AddBookDTO;
import ru.sber.spring.Java13SpringTU.library_project.dto.AuthorDTO;
import ru.sber.spring.Java13SpringTU.library_project.exception.MyDeleteException;
import ru.sber.spring.Java13SpringTU.library_project.mapper.AuthorMapper;
import ru.sber.spring.Java13SpringTU.library_project.model.Author;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;
import ru.sber.spring.Java13SpringTU.library_project.repository.AuthorRepository;

import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class AuthorService extends GenericService<Author, AuthorDTO> {
    private final AuthorRepository authorRepository;
    private final BookService bookService;
    protected AuthorService(AuthorRepository authorRepository, AuthorMapper authorMapper, BookService bookService) {
        super(authorRepository, authorMapper);
        this.authorRepository = authorRepository;
        this.bookService = bookService;
    }



// ДОБАВИТЬ КНИГУ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void addBook(AddBookDTO addBookDTO) {
        AuthorDTO author = getOne(addBookDTO.getAuthorId());
        bookService.getOne(addBookDTO.getBookId());
        author.getBooksIds().add(addBookDTO.getBookId());
        update(author);
    }



// ПОИСК АВТОРОВ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public Page<AuthorDTO> searchAuthors(final String fio, Pageable pageable) {
        Page<Author> authors = authorRepository.findAllByAuthorFioContainsIgnoreCase(fio, pageable);
        List<AuthorDTO> result = mapper.toDTOs(authors.getContent());
        return new PageImpl<>(result, pageable, authors.getTotalElements());
    }



// УДАЛЕНИЕ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void deleteSoft(Long objectId) throws MyDeleteException {
        Author author = authorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Автора с заданным id=" + objectId + " не существует."));
        boolean authorCanBeDeleted = authorRepository.checkAuthorForDeletion(objectId);
        if (authorCanBeDeleted) {
            markAsDeleted(author);
            Set<Book> books = author.getBooks();
            if (books != null && books.size() > 0) {
                books.forEach(this::markAsDeleted);
            }
            authorRepository.save(author);
        }
        else {
            throw new MyDeleteException(Errors.Authors.AUTHOR_DELETE_ERROR);
        }
    }
    public void restore(Long objectId) {
        Author author = authorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Автора с заданным id=" + objectId + " не существует."));
        unMarkAsDeleted(author);
        Set<Book> books = author.getBooks();
        if (books != null && books.size() > 0) {
            books.forEach(this::unMarkAsDeleted);
        }
        authorRepository.save(author);
    }
}
