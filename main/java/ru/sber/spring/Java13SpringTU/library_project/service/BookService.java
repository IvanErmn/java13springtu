package ru.sber.spring.Java13SpringTU.library_project.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import ru.sber.spring.Java13SpringTU.library_project.constants.Errors;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookDTO;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookSearchDTO;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookWithAuthorsDTO;
import ru.sber.spring.Java13SpringTU.library_project.exception.MyDeleteException;
import ru.sber.spring.Java13SpringTU.library_project.mapper.BookMapper;
import ru.sber.spring.Java13SpringTU.library_project.mapper.BookWithAuthorsMapper;
import ru.sber.spring.Java13SpringTU.library_project.model.Book;
import ru.sber.spring.Java13SpringTU.library_project.repository.BookRepository;
import ru.sber.spring.Java13SpringTU.library_project.utils.FileHelper;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookService extends GenericService<Book, BookDTO> {
    private final BookRepository bookRepository;
    private final BookWithAuthorsMapper bookWithAuthorsMapper;
    protected BookService(BookRepository bookRepository, BookMapper bookMapper, BookWithAuthorsMapper bookWithAuthorsMapper) {
        super(bookRepository, bookMapper);
        this.bookRepository = bookRepository;
        this.bookWithAuthorsMapper = bookWithAuthorsMapper;
    }



// СПИСКИ КНИГ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public Page<BookWithAuthorsDTO> getAllBooksWithAuthors(Pageable pageable) {
        Page<Book> booksPaginated = repository.findAll(pageable);
        List<BookWithAuthorsDTO> result = bookWithAuthorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }
    public Page<BookWithAuthorsDTO> getAllNotDeletedBooksWithAuthors(Pageable pageable) {
        Page<Book> booksPaginated = repository.findAllByIsDeletedFalse(pageable);
        List<BookWithAuthorsDTO> result = bookWithAuthorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }
    public BookWithAuthorsDTO getBookWithAuthors(Long id) {
        return bookWithAuthorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }
    public Page<BookWithAuthorsDTO> findBooks(BookSearchDTO bookSearchDTO, Pageable pageable) {
        String genre = bookSearchDTO.getGenre() != null ? String.valueOf(bookSearchDTO.getGenre().ordinal()) : null;
        Page<Book> booksPaginated = bookRepository.searchBooks(genre,
                bookSearchDTO.getTitle(),
                bookSearchDTO.getAuthorFio(),
                pageable
        );
        List<BookWithAuthorsDTO> result = bookWithAuthorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }



// СОЗДАТЬ И ОБНОВИТЬ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    // files/books/year/month/day/file_name_{id}_{created_when}.txt
    // files/книга_id.pdf - НЕуникальный путь
    public BookDTO create(final BookDTO object, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(bookRepository.save(mapper.toEntity(object)));
    }
    public BookDTO update(final BookDTO object, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }



// УДАЛЕНИЕ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void deleteSoft(Long id) throws MyDeleteException {
        Book book = repository.findById(id).orElseThrow(
                () -> new NotFoundException("Книги с заданным ID=" + id + " не существует"));
//        boolean bookCanBeDeleted = repository.findBookByIdAndBookRentInfosReturnedFalseAndIsDeletedFalse(id) == null;
        boolean bookCanBeDeleted = bookRepository.checkBookForDeletion(id);
        if (bookCanBeDeleted) {
            if (book.getOnlineCopyPath() != null && !book.getOnlineCopyPath().isEmpty()) {
                FileHelper.deleteFile(book.getOnlineCopyPath());
            }
            markAsDeleted(book);
            repository.save(book);
        }
        else {
            throw new MyDeleteException(Errors.Books.BOOK_DELETE_ERROR);
        }
    }
    public void restore(Long objectId) {
        Book book = repository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Книги с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(book);
        repository.save(book);
    }
}