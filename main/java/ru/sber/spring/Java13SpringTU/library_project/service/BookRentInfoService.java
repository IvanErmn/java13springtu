package ru.sber.spring.Java13SpringTU.library_project.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookDTO;
import ru.sber.spring.Java13SpringTU.library_project.dto.BookRentInfoDTO;
import ru.sber.spring.Java13SpringTU.library_project.mapper.BookRentInfoMapper;
import ru.sber.spring.Java13SpringTU.library_project.model.BookRentInfo;
import ru.sber.spring.Java13SpringTU.library_project.repository.BookRentInfoRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookRentInfoService extends GenericService<BookRentInfo, BookRentInfoDTO> {
    private final BookService bookService;
    private final BookRentInfoMapper bookRentInfoMapper;
    private final BookRentInfoRepository bookRentInfoRepository;


    protected BookRentInfoService(BookRentInfoRepository bookRentInfoRepository,
                                  BookRentInfoMapper bookRentInfoMapper,
                                  BookService bookService) {
        super(bookRentInfoRepository, bookRentInfoMapper);
        this.bookService = bookService;
        this.bookRentInfoMapper = bookRentInfoMapper;
        this.bookRentInfoRepository = bookRentInfoRepository;
    }

    public Page<BookRentInfoDTO> listUserRentBooks(final Long id,
                                                   final Pageable pageable) {
        Page<BookRentInfo> objects = bookRentInfoRepository.getBookRentInfoByUserId(id, pageable);
        List<BookRentInfoDTO> results = bookRentInfoMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public BookRentInfoDTO rentBook(BookRentInfoDTO rentBookDTO) {
        BookDTO bookDTO = bookService.getOne(rentBookDTO.getBookId());
        bookDTO.setAmount(bookDTO.getAmount() - 1);
        bookService.update(bookDTO);
        long rentPeriod = rentBookDTO.getRentPeriod() != null ? rentBookDTO.getRentPeriod() : 14L;
        rentBookDTO.setRentDate(LocalDateTime.now());
        rentBookDTO.setReturned(false);
        rentBookDTO.setRentPeriod((int) rentPeriod);
        rentBookDTO.setReturnDate(LocalDateTime.now().plusDays(rentPeriod));
        rentBookDTO.setCreatedWhen(LocalDateTime.now());
        rentBookDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(rentBookDTO)));
    }

    public void returnBook(final Long id) {
        BookRentInfoDTO bookRentInfoDTO = getOne(id);
        bookRentInfoDTO.setReturned(true);
        bookRentInfoDTO.setReturnDate(LocalDateTime.now());
        BookDTO bookDTO = bookRentInfoDTO.getBookDTO();
        bookDTO.setAmount(bookDTO.getAmount() + 1);
        update(bookRentInfoDTO);
        bookService.update(bookDTO);
    }
}