package ru.sber.spring.Java13SpringTU.library_project.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MySecuredAnnotation {
    String[] value();
}
