package ru.sber.spring.Java13SpringTU.library_project.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "books_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Book extends GenericModel {
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "description", nullable = false, length = 1000)
    private String description;
    @Column(name = "publish_date", nullable = false)
    private LocalDate localDate;
    @Column(name = "page_count")
    private Integer pageCount;
    @Column(name = "amount", nullable = false)
    private Integer amount;
    @Column(name = "storage_place", nullable = false)
    private String storagePlace;
    @Column(name = "publisher", nullable = false)
    private String publisher;
    @Column(name = "online_copy_path")
    private String onlineCopyPath;
    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;

    @ManyToMany
    @JoinTable(name = "books_authors",
            joinColumns = @JoinColumn(name = "book_id"), foreignKey = @ForeignKey(name = "FK_BOOK_AUTHORS"),
            inverseJoinColumns = @JoinColumn(name = "author_id"), inverseForeignKey = @ForeignKey(name = "FK_AUTHORS_BOOKS"))
//    @JsonBackReference
    private Set<Author> authors; // связь с классом Author

    @OneToMany(mappedBy = "book")
    private Set<BookRentInfo> bookRentInfos;

}
